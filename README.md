# ERTS20

Code and Models used for the paper "Towards Probabilistic Timing Analysis for SDFGs on Tile Based Heterogeneous MPSoCs" by Ralf Stemmer, Hai-Dang Vu, Kim Grüttner, Sebastien Le Nours, Wolfgang Nebel and Sebastien Pillement. Submitted on ERTS, 2020

The following list links to more of our repositories that contains additional code used in our experiments.

* JPEG-Decoder: https://gitlab.uni-oldenburg.de/stemmer/SDF-JPEG-Decoder
* Sobel-Filter: https://gitlab.uni-oldenburg.de/stemmer/SDF-Sobel-Filter
* Time measurement infrastructure: https://gitlab.uni-oldenburg.de/stemmer/time-measurement-infrastructure
* SystemC Model: https://gitlab.uni-oldenburg.de/stemmer/SystemC-Model
